package cn.edu.ncst.controller;

import java.util.HashMap;
import java.util.Map;

public class UserController {
    public static void main(String[] args) {
        System.out.println("模拟springboot中的接口功能");
        getUserInfo(new HashMap<String, Object>());
        userBind();
    }

    public static void getUserInfo(Map<String, Object> map) {
        System.out.println("前端好传来的数据接收成功了");
    }
    public static void userBind(){
        System.out.println("用户绑定接口实现");
    }
}