package cn.edu.ncst.mapper;

import cn.edu.ncst.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserMapper extends BaseMapper<User> {
}
