package cn.edu.ncst.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {
    private int age;
    private String name;
    private int sex;

}
