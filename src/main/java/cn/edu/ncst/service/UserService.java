package cn.edu.ncst.service;

import cn.edu.ncst.model.User;

public interface UserService {
    User getUserInfo();
}
